# Kotlin Compiler
Kotlin is a cross-platform, statically typed, general-purpose programming language with type inference.
Kotlin is designed to interoperate fully with Java, and the JVM version of Kotlin's standard library depends on the Java Class Library.

**Official Website:**: https://kotlinlang.org/

## Library version
This project is using Kotlin Compiler **1.7.0**.

## License

Kotlin Compiler is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

The **Trove** library is licensed under the [GNU Lesser General Public License, v2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html).

## Artifacts
The kotlin compiler artifacts are downloaded from the official repo:
https://github.com/JetBrains/kotlin/releases/tag/v1.7.0