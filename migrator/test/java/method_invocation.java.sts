/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.migrator.tests.java;

import * from "std/math/math";
open class SuperClass  {
    open foo(): void {
        System.out.println("Hi");
    }
}

open class SubClass1 extends SuperClass  {
    override foo(): void {
        throw new UnsupportedOperationException();
    }
    tweak : Runnable = new Runnable() {
        public override run(): void {
            SubClass1.super.foo(); // Gets the 'println' behavior
        }
    };
}

interface SuperInterface {
    foo(): void {
        System.out.println("Hi");
    }
}

open class SubClass2 implements SuperInterface  {
    public override foo(): void {
        throw new UnsupportedOperationException();
    }
    open tweak(): void {
        SuperInterface.super.foo(); // Gets the 'println' behavior
    }
}

open class SubClass3 implements SuperInterface  {
    public override foo(): void {
        throw new UnsupportedOperationException();
    }
    tweak : Runnable = new Runnable() {
        public override run(): void {
            SubClass3.SuperInterface.super.foo(); // Illegal
        }
    };
}

open class Doubler  {
    static two(): int {
        return two(1);
    }
    private static two(i : int): int {
        return 2 * i;
    }
}

open class Test extends Doubler  {
    static two(j : long): long {
        return j + j;
    }
    public static main(args : String[]): void {
        System.out.println(two(3));
        System.out.println(Doubler.two(3)); // Compile-time error
    }
}

open class ColoredPoint  {
    x : int ;
    y : int ;
    color : byte ;
    open setColor(color : byte): void {
        this.color = color;
    }
}

open class Test2  {
    public static main(args : String[]): void {
        let cp : ColoredPoint = new ColoredPoint();
        let color : byte = 37;
        cp.setColor(color);
        cp.setColor(37); // Compile-time error
    }
}

open class Point  {
    x : int ;
    y : int ;
}

open class ColoredPoint2 extends Point  {
    color : int ;
}

open class Test3  {
    static test(p : ColoredPoint, q : Point): void {
        System.out.println("(ColoredPoint, Point)");
    }
    static test(q : Point, p : ColoredPoint): void {
        System.out.println("(Point, ColoredPoint)");
    }
    public static main(args : String[]): void {
        let cp : ColoredPoint2 = new ColoredPoint2();
        test(cp, cp); // Compile-time error
    }
}

