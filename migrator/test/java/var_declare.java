/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.migrator.test.java;

class VarDeclareTest {
        
    public void Test() {
	int a;
	int b = 10;
	b *= 10;
	int c, d = 5;

	final double e = 2.781828;
	final float Pi = 3.14f;
	final double phi = 0.14e+01;
	final double g = 9.81e+00;
	final double dd = 4.343234234234234e+00;
    }
}
