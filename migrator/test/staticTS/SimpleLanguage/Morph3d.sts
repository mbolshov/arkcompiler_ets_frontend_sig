/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Morph3d {
  private static const param: int = 120;
  private static const n: int = 15;

  static a: double[];

  public setup(): void {
    a = new double[param * param * 3];
    for (let i: int = 0; i < param * param * 3; i++) {
      a[i] = 0;
    }
  }

  private morph(f: double): void {
    const paramPI2: double = Math.PI * 8 / param;
    const f30: double = -(50 * Math.sin(f * Math.PI * 2));

    for (let i: int  = 0; i < param; ++i) {
      for (let j: int = 0; j < param; ++j) {
        a[3 * (i * param + j) + 1] = Math.sin((j - 1) * paramPI2) * -f30;
      }
    }
  }

  public run(): void { //TBD: throws RuntimeException
    let loops: int = n;
    for (let i: int = 0; i < loops; i++) {
      morph(i / loops);
    }

    let testOutput: double = 0.0;
    for (let i: int = 0; i < param; i++) {
      testOutput += a[3 * (i * param + i) + 1];
    }
    let epsilon: double = 1e-13;
    if (Math.abs(testOutput) >= epsilon) {
      //System.err.println("ERROR: bad test output: expected magnitude below " + epsilon
      //                 + " but got " + testOutput);
      //System.exit(-1);
    }
    Consumer.consumeDouble(testOutput);
  }
}

